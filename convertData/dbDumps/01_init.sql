CREATE DATABASE vektis;

use vektis;

CREATE TABLE export_set (
    id INT(10) AUTO_INCREMENT PRIMARY KEY,
    bsn INT(9) NOT NULL,
    agb INT(8) NOT NULL,
    seen_when DATETIME NOT NULL,
    activity INT(10) NOT NULL
);

INSERT INTO export_set (bsn, agb, seen_when, activity) VALUES (123456789, 12345678, "2018-01-01 02:00:00", 1234567890);
insert into export_set (bsn, agb, seen_when, activity) VALUES (234567890, 12345678, "2017-12-30 11:30:00", 123456789);
insert into export_set (bsn, agb, seen_when, activity) VALUES (123456789, 12345678, "2017-12-30 11:30:00", 123456789);
